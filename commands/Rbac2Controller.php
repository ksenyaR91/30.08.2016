<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class RbacController extends Controller
{
	

	public function actionTmpermissions()
	{
		$auth = Yii::$app->authManager;
			
	}


	public function actionTlpermissions()
	{
		$auth = Yii::$app->authManager;
		
		$indexBonus = $auth->createPermission('indexBonus');
		$indexBonus->description = 'Teamleader can view bonus';
		$auth->add($indexBonus);
		
		$createBonus = $auth->createPermission('createBonus');
		$createBonus->description = 'teamleader can create new bonus';
		$auth->add($createBonus);
		
		$updateBonus = $auth->createPermission('updateBonus');
		$updateBonus->description = 'teamleader can update all bonus';
		$auth->add($updateBonus);
		
				
	}
	
	

	public function actionChilds()
	{
		$auth = Yii::$app->authManager;				
		
		$teammember = $auth->getRole('teammember');
		
		$teamleader = $auth->getRole('teamleader');
		$auth->addChild($teamleader, $teammember);

		$indexBonus = $auth->getPermission('indexBonus');
		$auth->addChild($teamleader, $indexBonus);

		$viewBonus = $auth->getPermission('viewBonus');
		$auth->addChild($teamleader, $viewBonus);
				
		$viewBonus = $auth->getPermission('viewBonus');
		$auth->addChild($teamleader, $viewBonus);
		
		$admin = $auth->getRole('teamleader');
		$auth->addChild($admin, $teamleader);
		
		
		
		
		
		
	}
}